﻿using System;

namespace Wing.Saga.Client
{
    [Serializable]
    public class UnitModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
